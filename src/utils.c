#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <front/lexer.h>
#include <utils.h>

void fatal_error(Fatal_Error type, const char* format, ...){
	va_list vl;
	va_start(vl, format);

	switch(type){
		case LOADER: fprintf(stderr, "Loader"); break;
		case LEXER: fprintf(stderr, "Lexer"); break;
		case PARSER: fprintf(stderr, "Parser");
	}

	fprintf(stderr, " error: ");
	vfprintf(stderr, format, vl);
	fprintf(stderr, "\n");
	
	lexer_free();
	va_end(vl);
	exit(1);
}
