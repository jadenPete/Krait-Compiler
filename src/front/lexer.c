#define PCRE2_CODE_UNIT_WIDTH 8

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <pcre2.h>

#include <front/lexer.h>
#include <utils.h>

const char* token_patterns[] = {
	"\\/\\/.*\\n|\\/\\*[\\s\\S]*\\*\\/",   // Comment
	"package|import",                      // Keyword
	"([a-zA-Z_]+[a-zA-Z0-9_]*)(\\.(?1))*", // Identifier
	"[\\n;]+",                             // Terminator
	" +"                                   // Whitespace
};

bool success = true;
char* source = NULL;
long int source_width;

pcre2_code* token_pattern = NULL;
pcre2_match_data* match_data = NULL;

Lexeme root_lexeme = {
	.source = {
		.offset = 0,
		.width = 0
	},

	.token = ROOT
};

Lexeme* lexemes = NULL;
Lexeme_Address lexemes_count;
Lexeme_Address lexemes_size;

const char* lexer_source_data(Source source_){
	return source + source_.offset;
}

size_t lexer_source_width(Source source_){
	return source_.width;
}

Token lexer_token(Lexeme lexeme){
	return lexeme.token;
}

Source lexer_source(Lexeme lexeme){
	return lexeme.source;
}

Lexeme_Address lexer_lexeme_count(void){
	return lexemes_count;
}

Lexeme lexer_lexeme(Lexeme_Address addr){
	return lexemes[addr];
}

void lexer_regex_error(int error_code){
	PCRE2_SIZE buffer_width = 256;
	PCRE2_UCHAR buffer[buffer_width];

	pcre2_get_error_message(error_code, buffer, buffer_width);
	fatal_error(LEXER, (char*) buffer);
}

void lexer_syntax_error(int offset, int width){
	success = false;

	fprintf(stderr, "Lexer error: Invalid syntax \"%.*s\"\n", width, source + offset);
}

void lexer_free_pcre(void){
	if(token_pattern != NULL){
		pcre2_code_free(token_pattern);
	}
	
	if(match_data != NULL){
		pcre2_match_data_free(match_data);
	}
}

void lexer_free(void){
	free(lexemes);
	free(source);
}

void lexer_realloc(void){
	if(++lexemes_count > lexemes_size){
		lexemes_size += 10;
		lexemes = realloc(lexemes, lexemes_size * sizeof(Lexeme));

		if(lexemes == NULL){
			fatal_error(LEXER, "Failed to reallocate lexemes buffer");
		}
	}
}

int lexer_sort_compare(const void* a, const void* b){
	Lexeme a_ = *(Lexeme*) a;
	Lexeme b_ = *(Lexeme*) b;

	if(a_.source.offset < b_.source.offset) return -1;
	if(a_.source.offset > b_.source.offset) return 1;

	if(a_.token < b_.token) return -1;
	if(a_.token > b_.token) return 1;

	return 0;
}

void lexer_sort(void){
	qsort(lexemes, lexemes_count, sizeof(Lexeme), lexer_sort_compare);
}

void lexer_discard(void){
	Lexeme_Address i;
	Lexeme_Address j;

	for(i = 0, j = 0; i < lexemes_count; i++){
		switch(lexemes[i].token){
			case COMMENT:
			case WHITESPACE:
			case OVERLAP:
				break;

			default:
				lexemes[j++] = lexemes[i];
		}
	}

	lexemes_count = j;
}

void lexer_check(void){
	if(lexemes_count == 0){
		lexer_syntax_error(0, source_width);
	} else {
		// Check for invalid syntax
		if(lexemes[0].source.offset > 0){
			lexer_syntax_error(0, lexemes[0].source.offset);
		}

		Lexeme_Address last = 0;
		Lexeme_Address current = 1;

		for(; current < lexemes_count; current++){
			size_t expected_offset = lexemes[last].source.offset + lexemes[last].source.width;
			size_t real_offset = lexemes[current].source.offset;

			// Check for invalid syntax or lexemes that overlap
			if(real_offset < expected_offset){
				lexemes[current].token = OVERLAP;
			} else {
				if(real_offset > expected_offset){
					lexer_syntax_error(expected_offset, real_offset - expected_offset);
				}
				
				last = current;
			}
		}

		size_t real_offset = lexemes[lexemes_count - 1].source.offset + lexemes[lexemes_count - 1].source.width;

		// Check for invalid syntax
		if(real_offset < (size_t) source_width){
			lexer_syntax_error(real_offset, source_width - real_offset);
		}
	}
}

void lexer_match(void){
	for(size_t i = 0; i < sizeof(token_patterns) / sizeof(*token_patterns); i++){
		int error_code;
		PCRE2_SIZE error_offset;
		
		token_pattern = pcre2_compile((PCRE2_SPTR) token_patterns[i], PCRE2_ZERO_TERMINATED, 0,
		                              &error_code, &error_offset, NULL);
		
		if(token_pattern == NULL){
			lexer_regex_error(error_code);
		}

		match_data = pcre2_match_data_create_from_pattern(token_pattern, NULL);

		PCRE2_SIZE subject_offset = 0;
		PCRE2_SIZE* ovector = pcre2_get_ovector_pointer(match_data);

		while(true){
			int match_result = pcre2_match(token_pattern, (PCRE2_SPTR) source, source_width,
			                               subject_offset, 0, match_data, NULL);

			if(match_result == PCRE2_ERROR_NOMATCH){
				break;
			}

			if(match_result < 0){
				lexer_regex_error(match_result);
			}

			lexer_realloc();

			lexemes[lexemes_count - 1] = (Lexeme) {
				.source = {
					.offset = ovector[0],
					.width = ovector[1] - ovector[0],
				},
				
				.token = i,
			};

			subject_offset = ovector[1];
		}

		lexer_free_pcre();
	}
}

bool lexer_open(const char* path){
	FILE* file = fopen(path, "r");

	if(file == NULL){
		fatal_error(LOADER, "Failed to open file");
	}

	fseek(file, 0, SEEK_END);
	source_width = ftell(file);
	source = malloc(source_width);
	fseek(file, 0, SEEK_SET);
	fread(source, 1, source_width, file);
	fclose(file);

	lexer_match();
	lexer_sort();
	lexer_check();
	lexer_discard();

	return success;
}
