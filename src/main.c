#include <front/lexer.h>
#include <front/syntax.h>

#include <main.h>
#include <utils.h>

int main(int argc, char** argv){
	if(argc == 2){
		if(lexer_open(argv[1])){
			parser_parse();
		}

		lexer_free();
	} else {
		fatal_error(LOADER, "Expected file path");
	}
}
