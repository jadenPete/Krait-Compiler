#ifndef FRONT_LEXER_H
#define FRONT_LEXER_H

#include <stdbool.h>
#include <stddef.h>

// Alphabetical, then by priority
typedef enum token {
	KEYWORD,
	IDENTIFIER,
	
	COMMENT,
	TERMINATOR,
	WHITESPACE,

	OVERLAP,
	ROOT
} Token;

typedef struct source Source;
typedef struct lexeme Lexeme;
typedef size_t Lexeme_Address;

struct source {
	size_t offset;
	size_t width;
};

struct lexeme {
	Source source;
	Token token;
};

const char* lexer_source_data(Source source_);
size_t lexer_source_width(Source source_);

Token lexer_token(Lexeme lexeme);
Source lexer_source(Lexeme lexeme);

Lexeme_Address lexer_lexeme_count(void);
Lexeme lexer_lexeme(Lexeme_Address addr);

void lexer_regex_error(int error_code);
void lexer_syntax_error(int offset, int width);

void lexer_free_pcre(void);
void lexer_free(void);
void lexer_realloc(void);

int lexer_sort_compare(const void* a, const void* b);
void lexer_sort(void);

void lexer_discard(void);
void lexer_check(void);
void lexer_match(void);

bool lexer_open(const char* path);

#endif
