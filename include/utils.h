#ifndef UTILS_H
#define UTILS_H

typedef enum fatal_error {
	LOADER, LEXER, PARSER
} Fatal_Error;

void fatal_error(Fatal_Error type, const char* format, ...);

#endif
